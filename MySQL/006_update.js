const mysql = require("mysql2");
  
const con = mysql.createConnection({
  host: "localhost",
  user: "test",
  database: "mydb",
  password: "1234512345"
});
 
con.connect(function(err) {
    if (err) throw err;
    var sql = "UPDATE customers SET address = 'Valley 345' WHERE address = 'Canyon 123'";
    con.query(sql, function (err, result) {
      if (err) throw err;
      console.log(result.affectedRows + " record(s) updated");
    });

    const sql1 = "SELECT * FROM customers WHERE address = 'Valley 345'";
    con.query(sql1, function(err, results) {
    if(err) console.log(err);
    console.log(results);
});
    con.end();  
});