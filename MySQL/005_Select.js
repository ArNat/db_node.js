const mysql = require("mysql2");
  
const connection = mysql.createConnection({
  host: "localhost",
  user: "test",
  database: "mydb",
  password: "1234512345"
});
 
connection.query("SELECT * FROM customers", //connection.execute(...)
  function(err, results, fields) {
    console.log(err);
    console.log(results); // собственно данные
    console.log(fields); // мета-данные полей 
});

const sql = `SELECT * FROM customers WHERE name=? AND address=?`;
const filter = ['Company Inc', 'Highway 37'];  
connection.query(sql, filter, function(err, results) {
    if(err) console.log(err);
    console.log(results);
});

const sql1 = "SELECT * FROM customers WHERE address LIKE 'S%'";
connection.query(sql1, function(err, results) {
    if(err) console.log(err);
    console.log(results);
});

connection.query("SELECT * FROM customers ORDER BY name", function (err, results) {
  if(err) console.log(err);
  console.log(results);
});

connection.end();