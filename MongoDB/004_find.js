const MongoClient = require("mongodb").MongoClient;

const url = "mongodb://127.0.0.1:27017/";

const mongoClient = new MongoClient(url);

async function run() {

try {

// Подключаемся к серверу

await mongoClient.connect();

const db = mongoClient.db("usersCollectionDB");


console.log("Подключение с сервером успешно установлено");

 const collection = db.collection("users");
 
 //const results = await collection.find().toArray();
 //const results = await collection.find({name:'Sergey'}).toArray();
 //const results = await collection.find({name:'Sergey', age:37}).toArray();
 //const results = await collection.findOne({name:'Sergey'});
 const results = await collection.findOne({age : {$lt : 30} });
 
 
 console.log(results);
 
}catch(err) {

console.log("Возникла ошибка");

console.log(err);

} finally {

// Закрываем подключение при завершении работы или при ошибке

await mongoClient.close();

console.log("Подключение закрыто");

}

}

run().catch(console.error);