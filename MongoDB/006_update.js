const MongoClient = require("mongodb").MongoClient;

const url = "mongodb://127.0.0.1:27017/";

const mongoClient = new MongoClient(url);

async function run() {

try {

// Подключаемся к серверу

await mongoClient.connect();

const db = mongoClient.db("usersCollectionDB");


console.log("Подключение с сервером успешно установлено");

 const collection = db.collection("users");
 
 //const results = await collection.findOneAndUpdate({age:31}, {$set:{age:25}},{returnDocument:"after"});
 //const results = await collection.updateMany({name:'Sergey'}, {$set:{name:"Peter"}});
 const results = await collection.updateOne({name:"Max"},{$set:{age:30, salary:2000}});
 //const results = await collection.drop();
 
 
 console.log(results);
 
}catch(err) {

console.log("Возникла ошибка");

console.log(err);

} finally {

// Закрываем подключение при завершении работы или при ошибке

await mongoClient.close();

console.log("Подключение закрыто");

}

}

run().catch(console.error);