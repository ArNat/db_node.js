const MongoClient = require("mongodb").MongoClient;
const persons = require('./persons');

const url = "mongodb://127.0.0.1:27017/";

const mongoClient = new MongoClient(url);

async function run() {

try {

// Подключаемся к серверу

await mongoClient.connect();

// обращаемся к базе данных admin

const db = mongoClient.db("usersCollectionDB1");

console.log("Подключение с сервером успешно установлено");

 const collection = db.collection("users");
 await collection.insertMany(persons);

}catch(err) {

console.log("Возникла ошибка");

console.log(err);

} finally {

// Закрываем подключение при завершении работы или при ошибке

await mongoClient.close();

console.log("Подключение закрыто");

}

}

run().catch(console.error);