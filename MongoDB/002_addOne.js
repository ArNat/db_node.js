const MongoClient = require("mongodb").MongoClient;

const url = "mongodb://127.0.0.1:27017/";

const mongoClient = new MongoClient(url);

async function run() {

try {

// Подключаемся к серверу

await mongoClient.connect();

const db = mongoClient.db("userCollectionDB");


console.log("Подключение с сервером успешно установлено");

 const collection = db.collection("users");
 const user = {firstName: "Tom", lastName:"Tom", age:28};
 const res = await collection.insertOne(user)
 console.log(res);
}catch(err) {

console.log("Возникла ошибка");

console.log(err);

} finally {

// Закрываем подключение при завершении работы или при ошибке

await mongoClient.close();

console.log("Подключение закрыто");

}

}

run().catch(console.error);